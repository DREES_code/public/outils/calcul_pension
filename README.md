# Présentation

Ce paquet `R` calcule les droits à pension de retraite sur cas types. A partir d'une description des caractéristiques individuelles et de la carrière des individus, les droits à retraite sont calculés.

Cet outil est illustratif, et ne prétend pas simuler parfaitement toutes les règles du système de retraite français. Il ne vise pas à remplacer un simulateur précis comme [M@rel](https://www.info-retraite.fr/portail-info/sites/PortailInformationnel/home/actualites-1/simulez-votre-retraite-gratuitem.html). Les fonctions du paquet sont utilisées telles quelles dans le modèle de microsimulation Trajectoire. Elles correspondent à un peu plus d'un tiers du programme total. 

Cet outil est présenté sous la forme d'un projet collaboratif, initié par la [direction de la recherche, des études et des évaluations (DREES)](https://drees.solidarites-sante.gouv.fr/), service statistique ministériel du ministère des Solidarités et de la Santé.

Ce projet est distribué sous licence EUPL.

# Pour démarrer

Le guide utilisateur dans /vignettes/GuideUtilisateur.html explique l'installation puis le fonctionnement du paquet.

# Fonctionnalités du projet
Ce projet vise à calculer les droits à retraite de cas types. Il repose sur un ensemble de paramètres du système de retraite (âges légaux, durées légales, valeurs de points, etc.) et un ensemble de fonctions.

Il couvre plusieurs régimes de retraite publics obligatoires français, mais pas l'intégralité. Des simplifications peuvent être faites dans le calcul de certains droits. Cet outil ne vise pas à reproduire dans le moindre détail les régles des régimes de retraite.

Le pas des données de carrière est annuel; le pas des dates de naissance et de liquidation est mensuel.

# De l'utilisation des cas types
Les cas types sont très intéressants d'un point de vue illustratif, ou pour bien comprendre des mécanismes. En revanche, leur utilisation est limitée pour l'évaluation de réformes. En effet, les cas types étudiés sont généralement très schématiques (pas de trous de carrières, progression linéaire), et cette approche ne permet pas de rendre compte de l'hétérogénéité des individus en terme d'accumulation de droits à retraite. Ainsi, l'étude du système de retraite n'est pas possible uniquement par cas types.
