definit_param <- function(){
  generations <- 1942:2000
  parametres =readRDS(system.file("extdata","parametres.Rds", package = "calculPension"))
  
  c(parametres, list(generations = generations))
}

# lecture de l'Excel pour création de bases propres (format long)
extraire_donnnes<-function(Excel_du_COR){
  
  onglets<-setdiff(readxl::excel_sheets(Excel_du_COR), "Dictionnaire de variables")
  
  # on définit pour chaque onglet la sélection ("range") qu'on veut lire dans la page de calcul
  aCharger<-data.table(
    onglet = onglets,
    selection = c("A1:B82","A1:R72",rep("A1:BJ52",28),"A1:C122",rep("A1:BJ52",5),"A1:B122",rep("A1:BJ52",10))
  )

  # lecture de chaque onglet, création d'un objet du même nom que l'onglet  
  for(i in 1:dim(aCharger)[1]){
    assign(
      value = as.data.table(
        readxl::read_xls(
          Excel_du_COR,
          sheet = aCharger[i, "onglet"][[1]],
          range = aCharger[i, "selection"][[1]])),
      x = make.names(tolower(aCharger[i, "onglet"][[1]]))
    )
  }
  
  # création d'une base format long de salaire relatif
  salaireRelatif<-
    rbindlist(
      lapply(
        make.names(tolower(grep("SALREL", onglets, value = TRUE))),
        function(base){
          melt(get(base), id.vars="age", variable.name = "generation", value.name = "salaireRelatif", variable.factor = FALSE)[
            ,generation:=as.integer(gsub("g","",generation))][
              ,casType:=regmatches(base, regexpr("[0-9]+", base))]
        }
      )
    )
  
  # création d'une base format long de taux de primes
  tauxPrimes<-
    rbindlist(
      lapply(
        make.names(tolower(grep("TXPRIM", onglets, value = TRUE))),
        function(base){
          melt(get(base), id.vars="age", variable.name = "generation", variable.factor = FALSE, value.name = "tauxPrimes")[
            ,generation:=as.integer(gsub("g","",generation))][
              ,casType := regmatches(base, regexpr("(?<=cas)[0-9]+", base, perl = TRUE))][
                ,hypothese := gsub("txprim\\.","",gsub("\\.cas[0-9]+\\.?","",base))]
        }
      )
    )
  tauxPrimes <- tauxPrimes[hypothese %like% "var"][
    ,hypSmpt:=as.numeric(gsub("_","\\.",gsub("var","",hypothese)))][
      ,hypothese:=NULL]
  
  # format long pour âge
  names(age) <- tolower(names(age))
  age <- melt(age, id.vars="generation", variable.factor = FALSE)

  list(
    age = age,
    espVie = espvie,
    salaireRelatif = salaireRelatif,
    tauxPrimes = tauxPrimes,
    is.cas9 = is.cas9,
    iss.cas8 = iss.cas8)
}

# a partir des bases contenant les hypothèses des cas-types, on crée les bases
# d'entrée du module de calcul dtId, dtIdAnEtat etc. 
mise_en_forme <- function(donnees, parametres){
  base = data.table(dateNaissance = anneeMois(generations))
  base <-parametres$ageDuree[
    grandeur=="AOD" & categorie == "Droit commun",
    .(dateNaissance, AOD_droitCommun = valeur)][base, on = "dateNaissance", roll = Inf]
  base <-parametres$ageDuree[
    grandeur=="AAD" & categorie == "Droit commun",
    .(dateNaissance, AAD_droitCommun = valeur)][base, on = "dateNaissance", roll = Inf]
  base <-parametres$ageDuree[
    grandeur=="AOD" & categorie == "Actif fonction publique",
    .(dateNaissance, AOD_actif = valeur)][base, on = "dateNaissance", roll = Inf]
  baseDroitCommun <- base[,.(ageLiq = seq(AOD_droitCommun, AAD_droitCommun, .25)), dateNaissance]
  
  baseDroitCommun[,casType:="1"]
  baseDroitCommun[, id := paste("Cas", casType,"gen",extraireAnnee(dateNaissance), "ageLiq",ageLiq)]
  dtId1 <- baseDroitCommun[, .(id, sexe="Homme", dateNaissance, dateLiq = dateNaissance+ageLiq, casType)]
  
  baseDroitCommun[,generation:= extraireAnnee(dateNaissance)]
  age[variable=="c1_deb"][baseDroitCommun, on="generation"]
  
  carriere<-dcast(age[variable %like% "c1_"], formula = "generation ~ variable")[
    baseDroitCommun, on="generation"][
      , `:=`(
        dateLiq = dateNaissance + ageLiq * 12L,
        date_deb = dateNaissance + c1_deb * 12L,
        date_debcad = dateNaissance + c1_debcad * 12L)
        ]
  carriere<-rbind(
    carriere[, .(
      annee = seq(extraireAnnee(date_deb), extraireAnnee(date_debcad), 1L)),
      .(id, date_deb, date_debcad, casType, generation = extraireAnnee(dateNaissance))
    ][,.(id, annee, casType, age = annee - generation, generation, 
         dureeEnMois = fcase(
      annee==extraireAnnee(date_deb), 12 - extraireMois(date_deb) + 1,
      annee==extraireAnnee(date_debcad), extraireMois(date_deb) - 1,
      default = 12))
      ][, etat:="Salarié du privé non cadre"],
    carriere[, .(
      annee = seq(extraireAnnee(date_debcad), extraireAnnee(dateLiq), 1L)),
      .(id, date_debcad, dateLiq, casType, generation = extraireAnnee(dateNaissance))
    ][,.(id, annee, casType, age = annee - generation,  generation,
         dureeEnMois = fcase(
      annee==extraireAnnee(date_debcad), 12 - extraireMois(date_debcad) + 1,
      # l'année de liquidation est géré par le paquet
      default = 12))
    ][, etat:="Salarié du privé cadre"])
  
  carriere<-salaireRelatif[carriere, on = .(age, casType, generation)]
  carriere[, .(id, annee, etat, remuneration := dureeEnMois/12 * salaireRelatif)]
  }

calculs_pensions <- function(basesEntree){
  
}

# en partant de base, on développe les années entre la dateDebut et la dateFin avec une durée en mois
# On donne l'etat_ durant cette période de temps
developpe_carriere <- function(base, nomAgeDebut, nomAgeFin, etat_){
  baseC<-copy(base)
  baseC[, dateDebut := dateNaissance + nomAgeDebut * 12L, env=list(nomAgeDebut = nomAgeDebut)]
  baseC[, dateFin := dateNaissance + nomAgeFin * 12L, env=list(nomAgeFin = nomAgeFin)]
  baseC[,c(nomAgeDebut, nomAgeFin):=NULL]

  baseC<-baseC[, .(
    annee = seq(extraireAnnee(dateDebut), extraireAnnee(dateFin), 1L)),
    .(id, dateDebut, dateFin)
  ][,.(id, annee, 
       dureeEnMois = fcase(
         annee==extraireAnnee(dateDebut), 12 - extraireMois(dateDebut) + 1,
         annee==extraireAnnee(dateFin) & rep(nomAgeFin=="ageLiq",.N), 12,
         annee==extraireAnnee(dateFin) & rep(nomAgeFin!="ageLiq",.N), extraireMois(dateFin) - 1,
         default = 12))
  ][, etatTravail:=etat_]
  baseC[dureeEnMois!=0]
}

filtreCasTypeAuTauxPlein <- function(resultats){
 
  resultats$dtIdPensionnes[!aSurcoteDansCaissePrincipale & !aDecoteDansCaissePrincipale][
    order(dateLiqCaissePrincipale)][,head(.SD,1), .(casType, generation)]$id
}
