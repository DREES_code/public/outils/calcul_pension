# /Copyright (C) 2021. Logiciel élaboré par l'État, via la Drees
# Auteurs : Bureau Retraites, Drees.
# Ce programme informatique a été développé par la Drees. 
# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en ajoutant une "issue" au projet ou en écrivant à DREES-CODE@sante.gouv.fr
# Ce logiciel est régi par la licence EUPL
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accepté les termes.


#' Récupère l'âge de liquidation
#' @export
getAgeLiq<-
  function(
    dtId,
    dtIdCaisse
  )
    dtId[,.(id,dateNaissance)][
      dtIdCaisse,j=.(id,caisse,dateLiq,dateNaissance),on="id"][
        ,age := as.integer(dateLiq-dateNaissance)/12]
    
  

#' Récupère la série de Smic
#' @param debut le début en année
#' @param fin la fin en année
#' @param paramSmic le fichier de paramètres nécessaire
#' @export
serieSmic<-
  function(
    debut,fin,
    paramSmic = parametres$smic
  ) {
    if (debut < 1951) warning("La série de Smic n'est pas disponible avant 1951")
    if (fin > 2070) warning("La série de Smic n'est pas disponible après 2070")
    if (debut>fin) warning("debut>fin")
    paramSmic[annee>=debut & annee<=fin,smic]
    }

#' Récupère la série de PSS
#' @param debut le début en année
#' @param fin la fin en année
#' @param paramPSS le fichier de paramètres nécessaire
#' @export
seriePSS<-
  function(
    debut,fin,
    paramPSS = parametres$pss
  ) {
    if (debut < 1949) warning("La série de PSS n'est pas disponible avant 1949")
    if (fin > 2070) warning("La série de PSS n'est pas disponible après 2070")
    if (debut>fin) warning("debut>fin")
    paramPSS[annee>=debut & annee<=fin,pss]
  }

#' Récupère la série de SMPT
#' @param debut le début en année
#' @param fin la fin en année
#' @param paramSMPT le fichier de paramètres nécessaire
#' @export
serieSMPT<-
  function(
    debut,fin,
    paramSMPT = parametres$smpt
  ) {
    if (debut < 1949) warning("La série de SMPT n'est pas disponible avant 1949")
    if (fin > 2100) warning("La série de SMPT n'est pas disponible après 2100")
    if (debut>fin) warning("debut>fin")
    paramSMPT[caisse=="Cnav" & annee>=debut & annee<=fin,smpt]
  }
